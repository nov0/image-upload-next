//import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';

import dynamic from 'next/dynamic'
import { useState } from 'react';

const Camera = dynamic(
    () => import('react-html5-camera-photo'),
    { ssr: false }
);

const isClient = typeof window !== 'undefined';

const CameraComponent = () => {

const [ captcuredImages, setCaptcuredImages] = useState([]);


  function handleTakePhoto(dataUri) {
    // Do stuff with the photo...
    setCaptcuredImages([...captcuredImages, dataUri]);
    console.log('takePhoto ---> ', captcuredImages);
  }

  function handleTakePhotoAnimationDone(dataUri) {
    // Do stuff with the photo...
    console.log('takePhoto ---> ', dataUri);
  }

  function handleCameraError(error) {
    console.log('handleCameraError', error);
  }

  function handleCameraStart(stream) {
    console.log('handleCameraStart');
  }

  function handleCameraStop() {
    console.log('handleCameraStop');
  }

    return (
        <>
        {isClient && <Camera
            onTakePhoto={(dataUri) => { handleTakePhoto(dataUri); }}
            onTakePhotoAnimationDone={(dataUri) => { handleTakePhotoAnimationDone(dataUri); }}
            onCameraError={(error) => { handleCameraError(error); }}
            //idealFacingMode={FACING_MODES.ENVIRONMENT}
            //idealResolution = {{width: 640, height: 480}}
            //imageType={IMAGE_TYPES.JPG}
            imageCompression={0.97}
            isMaxResolution={false}
            isImageMirror={false}
            isSilentMode={false}
            isDisplayStartCameraError={true}
            isFullscreen={false}
            sizeFactor={1}
            onCameraStart={(stream) => { handleCameraStart(stream); }}
            onCameraStop={() => { handleCameraStop(); }}
          />}
         
         {captcuredImages.map((image, index) => {
             console.log("image: ", image);
            return <span key={index}>
                <img width="100px" src={image} />
            </span>}
            )
        }

          <style jsx>{`
        .container {
          margin: 20px;
        }
        .react-html5-camera-photo {
          position: relative;
          text-align: center;
        }
        
        .react-html5-camera-photo > video {
          width: 768px;
        }
        
        .react-html5-camera-photo > img {
          width: 768px;
        }
        
        .react-html5-camera-photo > .display-error {
          width: 768px;
          margin: 0 auto;
        }
        
        @media(max-width:768px){
          .react-html5-camera-photo > video, .react-html5-camera-photo > img {
            width: 100%;
          }
          .react-html5-camera-photo > .display-error {
            width: 100%;
          }
        }
        
        /* fullscreen enable by props */
        .react-html5-camera-photo-fullscreen > video, .react-html5-camera-photo-fullscreen > img {
          width: 100vw;
          height:100vh;
        }
        .react-html5-camera-photo-fullscreen > video {
          object-fit: fill;
        }
        .react-html5-camera-photo-fullscreen > .display-error {
          width: 100vw;
          height:100vh;
        }
        #container-circles {
          position: absolute;
          left: 50%;
          bottom: 90px;
        }
        
        #outer-circle {
          position: absolute;
          left: -37px;
        
          height: 75px;
          width: 75px;
        
          /*
            opacity of 0.4
          */
          background-color: rgba(255, 255, 255, 0.4);
          border-radius: 50%;
        
          z-index: 1;
        }
        
        #inner-circle {
          position: absolute;
          left: 50%;
          top: 38px;
        
          height: 44px;
          width: 44px;
        
          background: white;
          border-radius: 50%;
        
          /*
           Offset the position correctly with
           minus half of the width and minus half of the height
          */
          margin: -22px 0px 0px -22px;
        
          /*
            On the top of outer-circle
          */
          z-index: 2;
        }
        
        #inner-circle.is-clicked {
          height: 38px;
          width: 38px;
          margin: -19px 0px 0px -19px;
        }
        #container-close-button {
          position: absolute;
          left: 80%;
          top: 10px;
          height: 0;
          width: 10px;
          z-index: 1;
          font-size: 20px;
          color: white;
          background-color: red;
          cursor: pointer;
        }
      `}</style>
          </>
    )
}

export default CameraComponent;