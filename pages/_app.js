import Head from 'next/head'
import 'react-html5-camera-photo/build/css/index.css';

export default function App({ Component, pageProps }) {
  return (
    <div>
        <Head>
            <title>Image uploader Demo</title>
            <link rel="icon" href="/favicon.ico" />
      </Head>
      <Component {...pageProps} />
    </div>
  )
}