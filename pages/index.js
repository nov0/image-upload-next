import React, { useState } from "react";
import CameraComponent from "../scr/CameraComponent";

export default function Home() {

  const [cameraOpened, setCamraOpened] = useState(false);

  return (
    <div className="container">
      <button tpype="button" onClick={() => setCamraOpened(true)}>Open camera</button>
      <button tpype="button" onClick={() => setCamraOpened(false)}>Close camera</button>
     { cameraOpened &&
        <CameraComponent />
      }
    </div>
  )
}
